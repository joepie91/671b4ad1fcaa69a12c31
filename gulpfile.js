/* Based on https://gist.github.com/spelufo/10872517 */

var gulp = require('gulp');
var child = require('child_process');
var childProcesses = {};

var jade = require("gulp-jade");

/* CoffeeScript compile deps */
var gutil = require('gulp-util');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var coffee = require('gulp-coffee');
var cache = require('gulp-cached');
var remember = require('gulp-remember');
var plumber = require('gulp-plumber');
 
//////////////////////////////////////////////////////////////// node-webkit
 
function startNodeWebkit () {
 
  if (childProcesses['node-webkit']) childProcesses['node-webkit'].kill();
 
  var nwProcess = childProcesses['node-webkit'] = child.spawn('./nw', ['.']);
 
  nwProcess.stderr.on('data', function (data) {
    var log = data.toString().match(/\[.*\]\s+(.*), source:.*\/(.*)/);
    if (log) process.stdout.write('[node] '+log.slice(1).join(' ')+'\n');
  });
 
}
 
gulp.task('node-webkit', startNodeWebkit);
 
// Press [ENTER] to manually restart nw.
process.stdin.on('data', function (data) {
  if (data.toString() === '\n') startNodeWebkit();
});

//////////////////////////////////////////////////////////////// coffeescript

gulp.task("coffee", function() {
	return gulp.src("./lib/**/*.coffee", {base: "./lib"})
		.pipe(plumber())
		.pipe(cache("coffee"))
		.pipe(coffee({bare: true}).on('error', gutil.log)).on('data', gutil.log)
		.pipe(remember("coffee"))
		.pipe(gulp.dest("./app/assets/js"))
});

/* For template utils and such. */

gulp.task("coffee2", function() {
	return gulp.src("./*.coffee", {base: "."})
		.pipe(plumber())
		.pipe(cache("coffee"))
		.pipe(coffee({bare: true}).on('error', gutil.log)).on('data', gutil.log)
		.pipe(remember("coffee"))
		.pipe(gulp.dest("."))
});

//////////////////////////////////////////////////////////////// jade

gulp.task("jade", function() {
	return gulp.src("./**/*.jade", {base: "."})
		.pipe(plumber())
		.pipe(cache("jade"))
		.pipe(jade({locals: require("./templateUtil")}))
		.pipe(remember("jade"))
		.pipe(gulp.dest("."))
});
 
//////////////////////////////////////////////////////////////// reload
 
var reload;
 
require('net').createServer(function(socket) {
  reload = socket;
}).listen(9292);
 
gulp.task('reload', function () {
  reload.write('reload');
});
 
//////////////////////////////////////////////////////////////// start
 
gulp.task('watch', function () {
  gulp.watch(['app/assets/css/*.css'], ['reload']);
  gulp.watch(['./lib/**/*.coffee'], ['coffee']);
  gulp.watch(['./*.coffee'], ['coffee2']);
  gulp.watch(['./**/*.jade'], ['jade']);
  gulp.watch(['app/assets/js/**/*.js', './**/*.html', 'package.json'], ['reload']);
});
 
gulp.task('default', ['coffee', 'coffee2', 'jade', 'node-webkit', 'watch']);
 
 
//////////////////////////////////////////////////////////////// finish
 
process.on('exit', function () {
  for (var c in childProcesses) childProcesses[c].kill();
});